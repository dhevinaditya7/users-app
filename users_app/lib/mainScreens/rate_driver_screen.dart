// ignore_for_file: prefer_const_constructors

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:smooth_star_rating_nsafe/smooth_star_rating.dart';
import 'package:users_app/global/global.dart';

class RateDriverScreen extends StatefulWidget {
  String? assignedDriverId;

  RateDriverScreen({this.assignedDriverId});
  @override
  State<RateDriverScreen> createState() => _RateDriverScreenState();
}

class _RateDriverScreenState extends State<RateDriverScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14),
        ),
        backgroundColor: Colors.white60,
        child: Container(
          margin: EdgeInsets.all(8),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white54,
            borderRadius: BorderRadius.circular(6),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const SizedBox(
                height: 22.0,
              ),
              Text(
                "Rate Trip Experience",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2,
                  fontSize: 22,
                  color: Colors.black54,
                ),
              ),
              SizedBox(
                height: 22.0,
              ),
              const Divider(
                height: 4.0,
                thickness: 4.0,
              ),
              SizedBox(
                height: 22.0,
              ),
              SmoothStarRating(
                rating: countRatingStars,
                allowHalfRating: true,
                color: Colors.green,
                borderColor: Colors.green,
                starCount: 5,
                onRatingChanged: (valueOfStarChoosed) {
                  countRatingStars = valueOfStarChoosed;
                  if (countRatingStars == 1) {
                    setState(() {
                      titleStarRating = "Very Bad";
                    });
                  }
                  if (countRatingStars == 2) {
                    setState(() {
                      titleStarRating = "Bad";
                    });
                  }
                  if (countRatingStars == 1) {
                    setState(() {
                      titleStarRating = "Good";
                    });
                  }
                  if (countRatingStars == 2) {
                    setState(() {
                      titleStarRating = "Very Good";
                    });
                  }
                  if (countRatingStars == 2) {
                    setState(() {
                      titleStarRating = "Excellent";
                    });
                  }
                },
              ),
              const SizedBox(
                height: 12.0,
              ),
              Text(
                titleStarRating,
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                ),
              ),
              const SizedBox(
                height: 18.0,
              ),
              ElevatedButton(
                onPressed: () {
                  DatabaseReference rateDriverRef = FirebaseDatabase.instance
                      .ref()
                      .child("drivers")
                      .child(widget.assignedDriverId!)
                      .child("ratings");

                  rateDriverRef.once().then((snap) {
                    //
                    if (snap.snapshot.value == null) {
                      rateDriverRef.set(countRatingStars.toString());

                      SystemNavigator.pop();
                    } else {
                      double pastRatings =
                          double.parse(snap.snapshot.value.toString());
                      double newAverageRatings =
                          (pastRatings + countRatingStars) / 2;
                      rateDriverRef.set(newAverageRatings.toString());

                      SystemNavigator.pop();
                    }

                    Fluttertoast.showToast(msg: "Please Restart App Now");
                  });
                },
                style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    padding: EdgeInsets.symmetric(horizontal: 74)),
                child: Text(
                  "Submit",
                  style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
              ),
              const SizedBox(
                height: 16.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
