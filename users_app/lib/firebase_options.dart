// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyDrzUzoaQ-23DUaEFpqJsiA_dgAyMHr2SY',
    appId: '1:1094911694673:web:d3409d575181ea122b2570',
    messagingSenderId: '1094911694673',
    projectId: 'uber-ola-and-indriver-cl-314f8',
    authDomain: 'uber-ola-and-indriver-cl-314f8.firebaseapp.com',
    databaseURL: 'https://uber-ola-and-indriver-cl-314f8-default-rtdb.firebaseio.com',
    storageBucket: 'uber-ola-and-indriver-cl-314f8.appspot.com',
    measurementId: 'G-J5V16ENWGM',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyBbXYROE0J7cLmSXSUiqi6OqTQb5a4gMeU',
    appId: '1:1094911694673:android:af08bb8461df75012b2570',
    messagingSenderId: '1094911694673',
    projectId: 'uber-ola-and-indriver-cl-314f8',
    databaseURL: 'https://uber-ola-and-indriver-cl-314f8-default-rtdb.firebaseio.com',
    storageBucket: 'uber-ola-and-indriver-cl-314f8.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyDpB0-IzytNlmwH3H68xjWPbQ86Wn-7So4',
    appId: '1:1094911694673:ios:5717bd9649eb604f2b2570',
    messagingSenderId: '1094911694673',
    projectId: 'uber-ola-and-indriver-cl-314f8',
    databaseURL: 'https://uber-ola-and-indriver-cl-314f8-default-rtdb.firebaseio.com',
    storageBucket: 'uber-ola-and-indriver-cl-314f8.appspot.com',
    iosClientId: '1094911694673-aujvgg3f1pcr1eoehp15n2pm9ns0ehuf.apps.googleusercontent.com',
    iosBundleId: 'com.example.usersApp',
  );
}
